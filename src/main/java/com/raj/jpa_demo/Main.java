package com.raj.jpa_demo;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * neon comment
 * @author rajendrat
 *
 */
public class Main {
	private static EntityManager entityManager;

	public static void main(String[] args) {
		Emp emp1 = new Emp("Raju");
		emp1.setAddress(new Address(0, "A"));
		entityManager = getEntityManager();
		EntityTransaction tr = entityManager.getTransaction();
		tr.begin();
		entityManager.persist(emp1);
		tr.commit();
		entityManager.clear();
		entityManager.close();
		
		entityManager = getEntityManager();
		Emp findEmp = entityManager.find(Emp.class, emp1.getId());
		entityManager.close();
		System.out.println("Find Emp: " + findEmp);
	}

	private static EntityManager getEntityManager() {
		if (entityManager == null || !entityManager.isOpen()) {
			entityManager = JPASessionUtil.getEntityManager("facultyjpa");
		}
		return entityManager;
	}
}
